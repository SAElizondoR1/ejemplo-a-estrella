class Nodo:
    def __init__(self, dato=None):
        self.dato = dato
        self.padre = None
        self.aristas = []
        self.costo = None

    def agregar_vecino(self, nodo):
        self.aristas.append(nodo)
